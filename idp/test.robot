*** Settings ***
Documentation  A test suite with a single test for valid login.
...  This test has a workflow that is created using keywords in
...  the imported resource file.
Resource  ../resources.robot

*** Variables ***

${host_spfw}  https://idp.dev.autentia.systems
${host_pfw}  http://localhost:3000
${AUTOIDENTIFY}  /authentication/autoidentify
${PASSWORD}  /authentication/password
${PROFILES}  /profiles/
${FACIAL}  /authentication/facial

*** Test Cases ***
SEARCH REGISTER
  FOR  ${dni}  IN  @{DATA.DNI}
  ${status}  SEARCH REGISTER  ${dni}
  Run Keyword And Continue On Failure  Should Be True  ${status}==200
  END

FACIAL-VALID DATA
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  ${count}=  Get length  ${DATA.DNI}
  FOR  ${index}  IN RANGE  ${count}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=*/*  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${file_path}  Catenate  SEPARATOR=  photos/  ${DATA.DNI[${index}]}  .png
    &{fileParts}=  Create Dictionary
    ${credential}  Create Dictionary  country=chl  iss=srcei  typ=rut  sub=${DATA.DNI[${index}]}
    ${credentialJSON}  Json.Dumps  ${credential}
    ${payload}  Create Dictionary  credential=${credentialJSON}
    Create Multi Part  ${fileParts}  face  ${file_path}  image/png
    ${response}=  Post Request  my_session  ${FACIAL}  data=${payload}  files=${fileParts}  headers=${headers}
    Run Keyword And Continue On Failure  Should Be True  ${response.status_code}==201
    Log  dni:${DATA.DNI[${index}]}   TraceId:${traceid}   codigo respuesta:${response.status_code}
  END

FACIAL-INVALID DATA
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  ${count}=  Get length  ${DATA.DNI}
  FOR  ${index}  IN RANGE  ${count}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=*/*  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${random_index}   Evaluate    random.choice([i for i in range(0,${count}) if i not in [${index}]])
    ${file_path}  Catenate  SEPARATOR=  photos/  ${DATA.DNI[${random_index}]}  .png
    &{fileParts}=  Create Dictionary
    ${payload}  Create Dictionary  rut=${DATA.DNI[${index}]}
    Create Multi Part  ${fileParts}  face  ${file_path}  image/png
    ${response}=  Post Request  my_session  ${FACIAL}  data=${payload}  files=${fileParts}  headers=${headers}
    Run Keyword And Continue On Failure  Should Be True  ${response.status_code}==400
    Log  dni:${DATA.DNI[${index}]} TraceId:${traceid} codigo respuesta: ${response.status_code}
  END

AUTOIDENTIFY-VALID DNI AND SERIE
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  ${count}=  Get length  ${DATA.DNI}
  FOR  ${index}  IN RANGE  ${count}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=application/json  Content-Type=application/json  charset=utf-8  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${payload}  Create Dictionary  sub=${DATA.DNI[${index}]}  serial=${DATA.SERIAL[${index}]}
    ${response}=  Post Request  my_session  ${AUTOIDENTIFY}  data=${payload}  headers=${headers}
    ${res_status}=  convert to string  ${response}
    Run Keyword Unless  "${DATA.SERIAL[${index}]}" == "0"  Run Keyword And Continue On Failure  should contain  ${res_status}  201
    Log  dni:${DATA.DNI[${index}]} TraceId:${traceid} codigo respuesta: ${response.status_code}
  END

AUTOIDENTIFY-VALID DNI AND INVALID SERIAL
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  ${count}=  Get length  ${DATA.DNI}
  FOR  ${index}  IN RANGE  ${count}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=application/json  Content-Type=application/json  charset=utf-8  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${payload}  Create Dictionary  sub=${DATA.DNI[${index}]}  serial=${DATA.INVALID_DATA[${index}]}
    ${response}=  Post Request  my_session  ${AUTOIDENTIFY}  data=${payload}  headers=${headers}
    ${res_status}=  convert to string  ${response}
    ${content}  To Json  ${response.content}
    Log  dni:${DATA.DNI[${index}]} TraceId:${traceid} codigo respuesta: ${response.status_code}

  END


PASSWORD-VALID DNI AND PASS
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  ${count}=  Get length  ${DATA.DNI}
  FOR  ${index}  IN RANGE  ${count}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=application/json  Content-Type=application/json  charset=utf-8  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${payload}  Create Dictionary  rut=${DATA.DNI[${index}]}  password=${DATA.PASSWORD[${index}]}
    ${response}=  Post Request  my_session  ${PASSWORD}  data=${payload}  headers=${headers}
    ${res_status}=  convert to string  ${response}
    Run Keyword Unless  "${DATA.PASSWORD[${index}]}" == "0"  Run Keyword And Continue On Failure  should contain  ${res_status}  201
    Log  dni:${DATA.DNI[${index}]} TraceId:${traceid} codigo respuesta: ${response.status_code}

    
  END

PASSWORD-VALID DNI AND INVALID PASS
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  ${count}=  Get length  ${DATA.DNI}
  FOR  ${index}  IN RANGE  ${count}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=application/json  Content-Type=application/json  charset=utf-8  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${payload}  Create Dictionary  rut=${DATA.DNI[${index}]}  password=${DATA.INVALID_DATA[${index}]}
    ${response}=  Post Request  my_session  ${PASSWORD}  data=${payload}  headers=${headers}
    ${res_status}=  convert to string  ${response}
    Run Keyword And Continue On Failure  should contain  ${res_status}  400
    Log  dni:${DATA.DNI[${index}]} TraceId:${traceid} codigo respuesta: ${response.status_code}
  END

SEARCH PROFILES-VALID UUID
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  FOR  ${item}  IN  @{data.UUDI}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=application/json  Content-Type=application/json  charset=utf-8  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${uri}=  Catenate  SEPARATOR=  ${PROFILES}  ${item}
    ${response}=  Get Request  my_session  ${uri}  headers=${headers}
    ${res_status}=  convert to string  ${response}
    Run Keyword And Continue On Failure  should contain  ${res_status}  200
    Log  UUDI:${item} TraceId:${traceid} codigo respuesta: ${response.status_code}
  END

SEARCH PROFILES-VALID DNI
  ${headers}  Create Dictionary  Accept=application/json  Content-Type=application/json  charset=utf-8  Authorization=${TOKEN}
  ${uri}=  Catenate  SEPARATOR=  ${PROFILES}  /search
  Create Session  my_session  ${host_${TYPE_REQUEST}}  verify=false  disable_warnings=1
  FOR  ${item}  IN  @{data.DNI}
    ${traceid} =    Generate Random String    16     [NUMBERS]abcdef
    ${headers}=  Create Dictionary  Accept=application/json  Content-Type=application/json  charset=utf-8  Authorization=${TOKEN}  x-b3-traceid=${traceid}
    ${params}  Create Dictionary  sub=${item}
    ${response}=  Get Request  my_session  ${uri}  params=${params}  headers=${headers}
    ${res_status}=  convert to string  ${response}
    Run Keyword And Continue On Failure  should contain  ${res_status}  200
    Log  dni:${item} TraceId:${traceid} codigo respuesta: ${response.status_code}
  END

*** Keywords ***
Create Multi Part  
  [Arguments]  ${addTo}  ${partName}  ${filePath}  ${contentType}  ${content}=${None}
  ${fileData}=  Run Keyword If  '''${content}''' != '''${None}'''  Set Variable  ${content}
  ...  ELSE  Get Binary File  ${filePath}
  ${fileDir}  ${filename}=  Split Path  ${filePath}
  ${partData}=  Create List  ${filename}  ${fileData}  ${contentType}
  Log  ${partData}
  Set To Dictionary  ${addTo}  ${partName}=${partData}


  